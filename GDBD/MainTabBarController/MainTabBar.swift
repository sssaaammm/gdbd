//
//  MainTabBar.swift
//  GDBD
//
//  Created by Samuel Wong on 14/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

protocol MainTabBarDelegate {
    func mainTabBarDelegate_didPressButtonAtIndex(_ index: Int)
}

class MainTabBar: UIView {
    static var tabBarHeight:CGFloat = 50.0
    
    @IBOutlet weak var item1: UIButton!
    @IBOutlet weak var item2: UIButton!
    @IBOutlet weak var item3: UIButton!
    
    var _delegate:MainTabBarDelegate?

    var tabBarButtons:[UIButton] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadViewFromNib() {
        super.loadViewFromNib("MainTabBar", forClass: MainTabBar.self)
        
        initialize()
    }
}

extension MainTabBar {
    func initialize() {
        self.backgroundColor = Theme.goodColour
        tabBarButtons = [item1, item2, item3]
        
        for i in 0..<tabBarButtons.count {
            let item = tabBarButtons[i]
            item.titleLabel?.setupAsTabBarLabel()
            item.backgroundColor = UIColor.clear
            item.setTitleColor(UIColor.black, for: UIControlState())
            item.addTarget(self, action: #selector(handleTabBarItemDidPress(_:)), for: .touchUpInside)
            
            let tabBarItem = TabBarItems.MenuItems[i]
            item.setTitle(tabBarItem.title, for: UIControlState())
        }
    }
}

//MARK: Item Press Handlers
extension MainTabBar {
    @objc func handleTabBarItemDidPress(_ sender: UIButton) {
        for item in tabBarButtons {
            item.titleLabel?.font = UIFont.standardFont(weight: .Regular)
        }
        sender.titleLabel?.font = UIFont.standardFont(weight: .Bold)
        
        _delegate?.mainTabBarDelegate_didPressButtonAtIndex(indexForButton(sender))
    }
}

extension MainTabBar {
    func indexForButton(_ button:UIButton) -> Int {
        switch button {
        case item1 :        return 0
        case item2 :        return 1
        case item3 :        return 2
        default : return -1
        }
    }
}
