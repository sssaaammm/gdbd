//
//  MainTabBarController.swift
//  GDBD
//
//  Created by Samuel Wong on 14/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

enum TabBarItems:Int {
    case addDot
    case dotList
    case calendar
    
    case count
    
    static var MenuItems:[TabBarItems] {
        get {
            var toReturn:[TabBarItems] = []
            for i in 0 ..< TabBarItems.count.rawValue {
                if let item = TabBarItems(rawValue: i) {
                    toReturn.append(item)
                }
            }
            
            return toReturn
        }
    }
    
    var title: String {
        switch self {
        case .addDot : return "Add"
        case .dotList : return "Dots"
        case .calendar : return "Calendar"
            
        case .count : return ""
        }
    }
    
    var storyboard:UIStoryboard! {
        var storyboard:String!
        switch self {
        case .addDot : storyboard = "Add"
        case .dotList : storyboard = "Dots"
        case .calendar : storyboard = "Calendar"
            
        case .count : return nil
        }
        
        return UIStoryboard(name: storyboard, bundle: Bundle.main)
    }
    
    var initialController:UIViewController! {
        guard self.storyboard != nil else { return nil }
        return self.storyboard.instantiateInitialViewController()
    }
    
    var image:UIImage {
        return UIImage()
    }
}

class MainTabBarController: UITabBarController {
    
    var mainTabBar:MainTabBar!
    
    let mainTabBarItems:[TabBarItems] = {
        return TabBarItems.MenuItems
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MainTabBarController {
    func initialize() {
        self.tabBar.isHidden = true
        self.selectedIndex = 0
        
        var _viewControllers:[UIViewController] = []
        
        for item in mainTabBarItems {
            _viewControllers.append(viewForTabBarItem(item))
        }
        
        self.setViewControllers(_viewControllers, animated: false)
        
        let heightOffset = MainTabBar.tabBarHeight - self.tabBar.frame.height
        mainTabBar = MainTabBar(frame: CGRect(x: self.tabBar.frame.origin.x, y: self.tabBar.frame.origin.y - heightOffset, width: self.tabBar.frame.size.width, height: MainTabBar.tabBarHeight))
        mainTabBar._delegate = self
        self.view.addSubview(mainTabBar)
    }
    
    fileprivate func viewForTabBarItem(_ item: TabBarItems) -> UIViewController! {
        if let initialView = item.initialController {
            initialView.tabBarItem = UITabBarItem(title: item.title, image: item.image, tag: item.rawValue)
            return initialView
        }
        
        print("FATAL ERROR : Could not find any views for - \(item.title)")
        return nil
    }
}

extension MainTabBarController: MainTabBarDelegate {
    func mainTabBarDelegate_didPressButtonAtIndex(_ index: Int) {
        self.selectedIndex = index
    }
}
