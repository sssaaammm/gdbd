//
//  GDBDViewController.swift
//  GDBD
//
//  Created by sarah burton on 20/3/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

class GDBDViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialise()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initialise() {
        self.view.backgroundColor = Theme.backgroundColour
    }
}
