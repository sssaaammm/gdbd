//
//  DateCollectionViewCell.swift
//  GDBD
//
//  Created by sarah burton on 20/2/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dotsContainingView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    
    var tempDotsContaingView: UIView!
    
    var dots: [Dot] = [] {
        didSet {
//            setUpViewForDots()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        initialise()
    }
}

extension DateCollectionViewCell {
    func initialise() {
        dateLabel.setupAsStandardLabel()
        self.backgroundColor = Theme.backgroundColour
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 4
    }
}

extension DateCollectionViewCell {
    func setUpViewForDots() {
        dotsContainingView.setNeedsLayout()
        dotsContainingView.layoutIfNeeded()
        
        tempDotsContaingView = UIView(frame: CGRect(origin: CGPoint.zero, size: dotsContainingView.frame.size))
        
        let numDotsPerRow = ceil(sqrt(Double(dots.count)))
        let dotWidth = (self.dotsContainingView.frame.width > self.dotsContainingView.frame.height ? self.dotsContainingView.frame.height : self.dotsContainingView.frame.width)/CGFloat(numDotsPerRow)
        let dotHeight = dotWidth
        for i in 0..<dots.count {
            let dot = dots[i]
            let rectangularView : UIView = UIView(frame: CGRect(x: CGFloat(Double(i).truncatingRemainder(dividingBy: numDotsPerRow))*dotWidth, y: CGFloat(floor(Double(i)/numDotsPerRow))*dotHeight, width: dotWidth, height: dotHeight))
            
            let dotView = UIView(frame: CGRect(x: (rectangularView.frame.width-rectangularView.frame.height)/2, y: 0, width: rectangularView.frame.height, height: rectangularView.frame.height))
            dotView.setUpAsCircle()
            
            dotView.backgroundColor = dot.isGood ? Theme.goodColour : Theme.badColour
            dotView.layer.borderColor = UIColor.black.cgColor
            dotView.layer.borderWidth = 0.5
            
            rectangularView.addSubview(dotView)
            tempDotsContaingView.addSubview(rectangularView)
//            dotsContainingView.addSubview(rectangularView)
        }
        
        dotsContainingView.addSubview(tempDotsContaingView)
    }
    
    func setUpViewForNoDots() {
        if tempDotsContaingView != nil {
            tempDotsContaingView.removeFromSuperview()
            tempDotsContaingView = nil
        }
//        for subview in dotsContainingView.subviews {
//            subview.removeFromSuperview()
//        }
    }
}

extension DateCollectionViewCell {
    override func prepareForReuse() {
        super.prepareForReuse()
        setUpViewForNoDots()
        dotsContainingView.isHidden = false
        dateLabel.isHidden = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setUpViewForNoDots()
        setUpViewForDots()
    }
}
