//
//  CalendarViewController.swift
//  GDBD
//
//  Created by sarah burton on 13/2/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import pop

class CalendarViewController: GDBDViewController {

    @IBOutlet weak var monthModeButton: UIButton!
    @IBOutlet weak var yearModeButton: UIButton!
    @IBOutlet weak var calendarModeSliderConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendarModeSliderView: UIView!
    @IBOutlet weak var monthPickerView: UIPickerView!
    @IBOutlet weak var yearPickerView: UIPickerView!
    @IBOutlet weak var monthCollectionView: UICollectionView!
    @IBOutlet weak var yearCollectionView: UICollectionView!
    @IBOutlet weak var monthCollectionViewLeftConstraint: NSLayoutConstraint!
    
    var year: Int! = Date().year
    var month: Date.Month! = Date.Month(rawValue: Date().month)
    var selectedIndexPath : IndexPath?
    var isMonthMode: Bool = true {
        didSet {
            isMonthModeDidChange()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        monthCollectionView.reloadData()
        yearCollectionView.reloadData()
    }
    
    override func initialise() {
        super.initialise()
        monthPickerView.dataSource = self
        monthPickerView.delegate = self
        yearPickerView.dataSource = self
        yearPickerView.delegate = self
        monthCollectionView.dataSource = self
        monthCollectionView.delegate = self
        monthCollectionView.backgroundColor = Theme.backgroundColour
        yearCollectionView.dataSource = self
        yearCollectionView.delegate = self
        yearCollectionView.backgroundColor = Theme.backgroundColour
        
        monthPickerView.selectRow(Date().month - 1, inComponent: 0, animated: false)
        
        monthModeButton.setUpAsLandingPageButton()
        monthModeButton.addTarget(self, action: #selector(monthModeButtonDidPress), for: .touchUpInside)
        
        yearModeButton.setUpAsLandingPageButton()
        yearModeButton.addTarget(self, action: #selector(yearModeButtonDidPress), for: .touchUpInside)
        
        monthModeButton.setTitle(Strings.month, for: .normal)
        yearModeButton.setTitle(Strings.year, for: .normal)
    }
}

extension CalendarViewController {
    @objc func monthModeButtonDidPress() {
        isMonthMode = true
    }
    @objc func yearModeButtonDidPress() {
        isMonthMode = false
    }
}

extension CalendarViewController {
    func isMonthModeDidChange() {
        monthPickerView.animateToHidden(isHidden: !isMonthMode)
        
        let monthViewAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
        monthViewAnimation.toValue = isMonthMode ? 0 : -self.view.frame.width
        monthViewAnimation.completionBlock = { (anim, error ) in
            if self.selectedIndexPath != nil {
                if let cell = self.yearCollectionView.cellForItem(at: self.selectedIndexPath!) {
                    cell.layer.borderColor = UIColor.clear.cgColor
                }
                if let cell = self.monthCollectionView.cellForItem(at: self.selectedIndexPath!) {
                    cell.layer.borderColor = UIColor.clear.cgColor
                }
            }
            self.selectedIndexPath = nil
        }
        monthCollectionViewLeftConstraint.pop_add(monthViewAnimation, forKey: "moveCalendarOver")
        
        let sliderAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
        sliderAnimation.toValue = isMonthMode ? 10 : calendarModeSliderView.frame.width + 20
        calendarModeSliderConstraint.pop_add(sliderAnimation, forKey: "moveItSlider")

    }
}

extension CalendarViewController {
    func numberYearRows() -> Int {
        let orderedDots = User.currentUser.dots.sorted(by: { $0.date > $1.date })
        if orderedDots.isEmpty {
            return 1
        } else {
            return Date().year - orderedDots.first!.date.year + 1
        }
    }
}

extension CalendarViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == monthPickerView {
            return Date.Month.Count.rawValue - 1
        } else {
            return numberYearRows()
        }
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if pickerView == monthPickerView {
            let monthLabel = UILabel()
            monthLabel.setupAsStandardLabel()
            monthLabel.textAlignment = .center
            if let month = Date.Month(rawValue: row + 1) {
                monthLabel.text = month.name
            } else {
                monthLabel.text = ""
            }
            return monthLabel
        } else {
            let yearLabel = UILabel()
            yearLabel.setupAsStandardLabel()
            yearLabel.textAlignment = .center
            let rowYear = Date().year - row
            yearLabel.text = "\(rowYear)"
            return yearLabel
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == monthPickerView {
            month = Date.Month(rawValue: row + 1)
        } else {
            year = Date().year - row
        }
        monthCollectionView.reloadData()
        yearCollectionView.reloadData()
    }
}


extension CalendarViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == monthCollectionView {
            return 7*6
        }
        return 3*4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCollectionViewCellIdentifier", for: indexPath) as! DateCollectionViewCell
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        if collectionView == monthCollectionView {
            let labelNumber = indexPath.row - (Date.date_monthStartDate(month.rawValue, year: year).dayOfWeek())! + 2
            cell.dateLabel.text = "\(labelNumber)"
            cell.dateLabel.isHidden = labelNumber <= 0 || labelNumber > (Date.date_monthEndDate(month.rawValue, year: year).day)
            let cellDate = Date.date_with(day: labelNumber, month: month.rawValue, year: year)
            cell.dots = User.currentUser.dots.filter( { $0.date.standardizeDate() == cellDate.standardizeDate() } )
            cell.dotsContainingView.isHidden = labelNumber <= 0 || labelNumber > (Date.date_monthEndDate(month.rawValue, year: year).day)
            cell.layer.borderColor = selectedIndexPath == indexPath && 0 < labelNumber && labelNumber <= (Date.date_monthEndDate(month.rawValue, year: year).day) ? UIColor.black.cgColor : UIColor.clear.cgColor
        } else {
            if let labelMonth = Date.Month(rawValue: indexPath.row + 1) {
                cell.dateLabel.text = labelMonth.name
            }
            cell.layer.borderColor = selectedIndexPath == indexPath ? UIColor.black.cgColor : UIColor.clear.cgColor
            cell.dots = User.currentUser.dots.filter( { $0.date.month == indexPath.row + 1})
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedIndexPath != indexPath {
            if selectedIndexPath != nil {
                if let cell = collectionView.cellForItem(at: selectedIndexPath!) {
                    cell.layer.borderColor = UIColor.clear.cgColor
                }
            }
            
            selectedIndexPath = indexPath
            
            if selectedIndexPath != nil {
                if let cell = collectionView.cellForItem(at: selectedIndexPath!) {
                    let labelNumber = indexPath.row - (Date.date_monthStartDate(month.rawValue, year: year).dayOfWeek())! + 2
                    if 0 < labelNumber && labelNumber <= (Date.date_monthEndDate(month.rawValue, year: year).day) {
                        cell.layer.borderColor = UIColor.black.cgColor
                    }
                }
            }
        } else {
            if let vc = ViewDotsModalViewController.viewDotsModalViewController {
                let dateNumber = indexPath.row - (Date.date_monthStartDate(month.rawValue, year: year).dayOfWeek())! + 2
                let cellDate = Date.date_with(day: dateNumber, month: month.rawValue, year: year)
                vc.dots = isMonthMode ? User.currentUser.dots.filter( { $0.date.standardizeDate() == cellDate.standardizeDate() } ) : User.currentUser.dots.filter( { $0.date.month == indexPath.row + 1})
                if collectionView == monthCollectionView {
                    vc.titleString = "\(cellDate.day) \(month.name)"
                } else {
                    if let modalMonth = Date.Month(rawValue: indexPath.row + 1) {
                        vc.titleString = modalMonth.name
                    }
                }
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == monthCollectionView {
            return CGSize(width: collectionView.frame.width/7.0, height: collectionView.frame.height/6.0)
        } else {
            return CGSize(width: collectionView.frame.width/3.0, height: collectionView.frame.height/4.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
