//
//  AddViewController.swift
//  GDBD
//
//  Created by sarah burton on 18/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import pop
import SVProgressHUD

class AddViewController: GDBDViewController {

    enum DotsViewState: Int {
        case Closed = 0
        case Good
        case Bad
        
        var boolean : Bool {
            get {
                switch self {
                case .Closed :
                    return false
                case .Good :
                    return true
                case .Bad :
                    return false
                }
            }
        }
    }
    
    @IBOutlet weak var goodDotButton: UIButton!
    @IBOutlet weak var badDotButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var goodDotButtonVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var goodDotButtonHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var badDotButtonVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var badDotButtonHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var textBoxVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var textBox: UIView!
    @IBOutlet weak var textBoxTitle: UILabel!
    @IBOutlet weak var textBoxField: UITextField!
    @IBOutlet weak var textBoxSeperator: UIView!
    @IBOutlet weak var commitButton: UIButton!
    
    var currentViewState: DotsViewState = .Closed {
        didSet {
            handleCurrentViewStateChanged()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func initialise() {
        super.initialise()
        
        goodDotButton.setUpAsDotButton(isGood: true)
        badDotButton.setUpAsDotButton(isGood: false)
        
        closeButton.alpha = 0
        textBox.alpha = 0
        
        closeButton.setTitleColor(UIColor.black, for: .normal)
        
        goodDotButton.addTarget(self, action: #selector(dotButtonDidPress(sender:)), for: .touchUpInside)
        badDotButton.addTarget(self, action: #selector(dotButtonDidPress(sender:)), for: .touchUpInside)
        closeButton.addTarget(self, action: #selector(closeButtonDidPress), for: .touchUpInside)
        commitButton.addTarget(self, action: #selector(commitButtonDidPress), for: .touchUpInside)
        
        let quarterFrameHeight = -view.frame.height/4
        let textBoxFrameHeight = textBox.frame.height/2
        let buttonFrameHeight = goodDotButton.frame.height/2
        
        textBoxVerticalConstraint.constant = quarterFrameHeight + textBoxFrameHeight + buttonFrameHeight + 50

        
        commitButton.setUpAsDisabledButton()
        commitButton.setTitle("Commit", for: .normal)
        
        textBox.backgroundColor = UIColor.white
        textBox.layer.borderColor = UIColor.black.cgColor
        textBox.layer.borderWidth = 1
        textBox.layer.cornerRadius = 4
        textBox.layer.masksToBounds = true
        
        textBoxField.delegate = self
    }
}

//MARK: Button Handlers
extension AddViewController {
    
    @objc func dotButtonDidPress(sender: UIButton) {
        switch sender {
        case goodDotButton :
            currentViewState = .Good
        case badDotButton :
            currentViewState = .Bad
        default : break
        }
    }
    
    @objc func closeButtonDidPress() {
        currentViewState = .Closed
        self.view.endEditing(true)
    }
    
    @objc func commitButtonDidPress() {
        self.view.endEditing(true)
        
        if String.isNilOrEmpty(string: textBoxField.text) {
            UIAlertController.showAlertWithError(viewController: self, errorString: Strings.missing_fields)
        } else {
            let dot = Dot()
            dot.isGood = currentViewState.boolean
            dot.eventDetails = textBoxField.text
            dot.date = Date()
            SVProgressHUD.show(withStatus: APIStrings.saving)
            dot.create({ (error) in
                SVProgressHUD.dismiss()
                guard error == nil else { UIAlertController.showAlertWithError(viewController: self, error: error!); return }
                self.currentViewState = .Closed
            })
        }
    }

    func handleCurrentViewStateChanged() {
        commitButton.setUpAsDisabledButton()
        if currentViewState == .Closed {
            goodDotButton.animateToHidden(isHidden: false)
            badDotButton.animateToHidden(isHidden: false)
            closeButton.animateToHidden(isHidden: true)
            textBox.animateToHidden(isHidden: true)
            textBoxField.text = ""
            
            let horizontalGoodDotAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
            horizontalGoodDotAnimation.toValue = 0
            goodDotButtonHorizontalConstraint.pop_add(horizontalGoodDotAnimation, forKey: "moveItBackGoodHorizontal")
            
            let horizontalBadDotAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
            horizontalBadDotAnimation.toValue = 0
            badDotButtonHorizontalConstraint.pop_add(horizontalBadDotAnimation, forKey: "moveItBackBadHorizontal")
            
            let verticalGoodDotAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
            verticalGoodDotAnimation.toValue = 0
            goodDotButtonVerticalConstraint.pop_add(verticalGoodDotAnimation, forKey: "moveItBackGoodVertical")
            
            let verticalBadDotAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
            verticalBadDotAnimation.toValue = 0
            badDotButtonVerticalConstraint.pop_add(verticalBadDotAnimation, forKey: "moveItBackBadVertical")
            
        } else {
            let horizontalAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
            horizontalAnimation.toValue = currentViewState == .Good ? self.view.frame.width/4 : -self.view.frame.width/4
            let horizontalConstraintToAnimate = currentViewState == .Good ? goodDotButtonHorizontalConstraint : badDotButtonHorizontalConstraint
            horizontalConstraintToAnimate?.pop_add(horizontalAnimation, forKey: "moveItHorizontal")
            
            let verticalAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
            verticalAnimation.toValue = -self.view.frame.height/4
            let verticalConstraintToAnimate = currentViewState == .Good ? goodDotButtonVerticalConstraint : badDotButtonVerticalConstraint
            verticalConstraintToAnimate?.pop_add(verticalAnimation, forKey: "moveItVertical")
            
            let dotToAlpha = currentViewState == .Good ? badDotButton : goodDotButton
            dotToAlpha?.animateToHidden(isHidden: true)
            
            closeButton.animateToHidden(isHidden: false)
            textBox.animateToHidden(isHidden: false, isDelayed: 0.3)
        }
    }
}

extension AddViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            if updatedString == "" {
                commitButton.setUpAsDisabledButton()
            } else {
                commitButton.setUpAsCommitButton(isGood: currentViewState.boolean)
            }
        
        return true
    }
}
