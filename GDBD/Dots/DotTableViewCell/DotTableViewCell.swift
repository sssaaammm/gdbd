//
//  DotTableViewCell.swift
//  GDBD
//
//  Created by sarah burton on 2/2/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import pop

protocol DotTableViewCellDelegate {
    func DotTableViewCellDelegate_deleteButtonDidPress(dot : Dot)
    func DotTableViewCellDelegate_moveDeleteView(dot : Dot, isOpen : Bool)
}

class DotTableViewCell: UITableViewCell {

    static let nib = UINib(nibName: "DotTableViewCell", bundle: nil)
    static let reuseIdentifier = "DotTableViewCellIdentifier"
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var eventLabel: UILabel!
    @IBOutlet weak var dotProxy: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var mainViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var seperator: UIView!
    
    var _delegate : DotTableViewCellDelegate?
    
    var dot : Dot! {
        didSet {
            dateLabel.text = dot.date.formatDateFromUTC_Readable_DayMonthYear() + ", " + dot.date.formatDateFromUTC_Readable_T()
            eventLabel.text = dot.eventDetails
            dotProxy.backgroundColor = dot.isGood ? Theme.goodColour : Theme.badColour
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        initialise()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension DotTableViewCell {
    func initialise() {
        self.selectionStyle = .none
        self.backgroundColor = Theme.backgroundColour
        
        dateLabel.setupAsStandardLabel()
        eventLabel.setupAsStandardLabel()
        dotProxy.setUpAsCircle()
        seperator.setUpAsSeperator()
        
        deleteButton.setTitle(Strings.delete, for: .normal)
        deleteButton.addTarget(self, action: #selector(deleteButtonDidPress), for: .touchUpInside)
        deleteButton.setTitleColor(UIColor.white, for: .normal)
        
        let swipeGestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(viewDidSwipeLeft))
        swipeGestureLeft.direction = .left
        
        let swipeGestureRight = UISwipeGestureRecognizer(target: self, action: #selector(viewDidSwipeRight))
        swipeGestureRight.direction = .right
        
        self.addGestureRecognizer(swipeGestureLeft)
        self.addGestureRecognizer(swipeGestureRight)
    }
}

extension DotTableViewCell {
    @objc func viewDidSwipeLeft() {
        print("swiped left")
        moveDeleteView(isOpen: true)
    }
    @objc func viewDidSwipeRight() {
        print("swiped right")
        moveDeleteView(isOpen: false)
    }
}

extension DotTableViewCell {
    func moveDeleteView(isOpen: Bool) {
        let deleteConstraintAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
        deleteConstraintAnimation.toValue = isOpen ? 100 : 0
        mainViewRightConstraint.pop_add(deleteConstraintAnimation, forKey: "moveDeleteButton")
        
        _delegate?.DotTableViewCellDelegate_moveDeleteView(dot: dot, isOpen: isOpen)
    }
}

extension DotTableViewCell {
    @objc func deleteButtonDidPress() {
        _delegate?.DotTableViewCellDelegate_deleteButtonDidPress(dot: dot)
    }
}
