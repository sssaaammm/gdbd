//
//  DotsViewController.swift
//  GDBD
//
//  Created by sarah burton on 30/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import SVProgressHUD

class DotsViewController: GDBDViewController {

    @IBOutlet weak var dotsTableView: UITableView!
    var openDots : [Dot] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        dotsTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func initialise() {
        super.initialise()
        dotsTableView.register(DotTableViewCell.nib, forCellReuseIdentifier: DotTableViewCell.reuseIdentifier)
        dotsTableView.delegate = self
        dotsTableView.dataSource = self
        dotsTableView.separatorStyle = .none
        dotsTableView.backgroundColor = Theme.backgroundColour
    }
}

extension DotsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return User.currentUser.dots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dot = User.currentUser.dots[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: DotTableViewCell.reuseIdentifier) as! DotTableViewCell
        cell.dot = dot
        cell._delegate = self
        cell.mainViewRightConstraint.constant = openDots.contains(where: { $0.id == dot.id}) ? 100 : 0
        return cell
    }
}

extension DotsViewController : DotTableViewCellDelegate {
    func DotTableViewCellDelegate_deleteButtonDidPress(dot: Dot) {
        SVProgressHUD.show(withStatus: "Deleting...")
        dot.delete { (error) in
            SVProgressHUD.dismiss()
            guard error == nil else { UIAlertController.showAlertWithError(viewController: self, error: error!); return }
            self.dotsTableView.reloadData()
        }
    }
    
    func DotTableViewCellDelegate_moveDeleteView(dot: Dot, isOpen: Bool) {
        if isOpen {
            openDots.append(dot)
        } else {
            if let index = openDots.index(where: { $0.id == dot.id }) {
                openDots.remove(at: index)
            }
        }
    }
}
