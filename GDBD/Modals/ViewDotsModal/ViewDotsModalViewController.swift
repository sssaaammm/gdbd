//
//  ViewDotsModalViewController.swift
//  GDBD
//
//  Created by sarah burton on 1/3/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import SVProgressHUD

class ViewDotsModalViewController: UIViewController {

    static var viewDotsModalViewController: ViewDotsModalViewController? {
        get {
            let storyboard = UIStoryboard(name: "ViewDotsModal", bundle: nil)
            return storyboard.instantiateInitialViewController() as? ViewDotsModalViewController
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var titleString: String!
    var dots: [Dot] = []
    var openDots: [Dot] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialise()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ViewDotsModalViewController {
    func initialise() {
        self.setUpWithModalTopBar(titleText: titleString)
        tableView.register(DotTableViewCell.nib, forCellReuseIdentifier: DotTableViewCell.reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
    }
}

extension ViewDotsModalViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dot = User.currentUser.dots[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: DotTableViewCell.reuseIdentifier) as! DotTableViewCell
        cell.dot = dot
        cell._delegate = self
        cell.mainViewRightConstraint.constant = openDots.contains(where: { $0.id == dot.id}) ? 100 : 0
        return cell
    }
}

extension ViewDotsModalViewController: DotTableViewCellDelegate {
    func DotTableViewCellDelegate_deleteButtonDidPress(dot: Dot) {
        SVProgressHUD.show(withStatus: "Deleting...")
        dot.delete { (error) in
            SVProgressHUD.dismiss()
            guard error == nil else { UIAlertController.showAlertWithError(viewController: self, error: error!); return }
            self.tableView.reloadData()
        }
    }
    
    func DotTableViewCellDelegate_moveDeleteView(dot: Dot, isOpen: Bool) {
        if isOpen {
            openDots.append(dot)
        } else {
            if let index = openDots.index(where: { $0.id == dot.id }) {
                openDots.remove(at: index)
            }
        }
    }
}
