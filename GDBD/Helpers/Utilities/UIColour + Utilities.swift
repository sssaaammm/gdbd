//
//  UIColour + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 29/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
    
extension UIColor {
    class func colourWithHexStringAndAlpha(_ hex: String, alpha:CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = cString.substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        let gString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 2)).substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        let bString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 4)).substring(to: cString.characters.index(cString.startIndex, offsetBy: 2))
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        let red:CGFloat = CGFloat(r) / 255.0
        let green:CGFloat = CGFloat(g) / 255.0
        let blue:CGFloat = CGFloat(b) / 255.0
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    class func colourWithHexString(_ hex: String) -> UIColor {
        return UIColor.colourWithHexStringAndAlpha(hex, alpha: 1)
    }
}
