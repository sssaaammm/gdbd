//
//  Date + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 5/2/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import Foundation

extension Date {
    enum Month : Int {
        case January = 1
        case February
        case March
        case April
        case May
        case June
        case July
        case August
        case September
        case October
        case November
        case December
        
        case Count
        
        var name : String {
            get {
                switch self {
                case .January :
                    return "January"
                case .February :
                    return "February"
                case .March :
                    return "March"
                case .April :
                    return "April"
                case .May :
                    return "May"
                case .June :
                    return "June"
                case .July :
                    return "July"
                case .August :
                    return "August"
                case .September :
                    return "September"
                case .October :
                    return "October"
                case .November :
                    return "November"
                case .December :
                    return "December"
                case .Count :
                    return ""
                }
            }
        }
        
    }
}

extension Date {
    func dayOfWeek() -> Int? {
        let cal: Calendar = Calendar.current
        let comp: DateComponents = (cal as NSCalendar).components(.weekday, from: self)
        return comp.weekday
    }
    
    var day:Int {
        get {
            return (Calendar.current as NSCalendar).component(.day, from: self)
        }
    }
    
    var month:Int {
        get {
            return (Calendar.current as NSCalendar).component(.month, from: self)
        }
    }
    
    var year:Int {
        get {
            return (Calendar.current as NSCalendar).component(.year, from: self)
        }
    }
    
    func isGreaterThanDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending
        {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    
    func isLessThanDate(_ dateToCompare : Date) -> Bool
    {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending
        {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func isGreaterThanOrEqualToDate(_ dateToCompare : Date) -> Bool {
        return self.isGreaterThanDate(dateToCompare) || (self == dateToCompare)
    }
    
    func isLessThanOrEqualToDate(_ dateToCompare : Date) -> Bool {
        //FIXME: Was isGreaterThanDate
        return self.isLessThanDate(dateToCompare) || (self == dateToCompare)
    }
    
    func asMidnightDate() -> Date {
        var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        calendar.timeZone = TimeZone(abbreviation: "UTC")!
        let components = (calendar as NSCalendar).components([.day, .month, .year, .hour, .minute], from: self)
        return calendar.date(from: components)!
    }
    
    func standardizeDate() -> Date {
        let calendar = Calendar.current
        let date = calendar.date(bySettingHour: 12, minute: 0, second: 0, of: self)
        return date!
    }
    
    static var currentDay:Int {
        return (Calendar.current as NSCalendar).component(.day, from: Date())
    }
    
    static var currentMonth:Int {
        return (Calendar.current as NSCalendar).component(.month, from: Date())
    }
    
    static var currentYear:Int {
        return (Calendar.current as NSCalendar).component(.year, from: Date())
    }
    
    static func lastDayInMonth(_ month:Int, year:Int) -> Int {
        var components = DateComponents()
        components.month = month
        components.year = year
        if let date = Calendar.current.date(from: components) {
            let range = (Calendar.current as NSCalendar).range(of: .day, in: .month, for: date)
            return range.length
        }
        
        return 31
    }
    
    static func date_with(day: Int!, month:Int! = nil, year:Int! = nil) -> Date {
        var components = DateComponents()
        components.day = day
        components.month = month != nil ? month : currentMonth
        components.year = year != nil ? year : currentYear
        let date = Calendar.current.date(from: components)
        
        return date!
    }
    
    static func date_monthStartDate(_ month:Int! = nil, year:Int! = nil) -> Date {
        var components = DateComponents()
        components.day = 1
        components.month = month != nil ? month : currentMonth
        components.year = year != nil ? year : currentYear
        components.timeZone = TimeZone.current
        components.hour = 12
        let date = Calendar.current.date(from: components)
        
        return date!
    }
    
    static func date_monthEndDate(_ month:Int! = nil, year:Int! = nil) -> Date {
        let _month = month != nil ? month : currentMonth
        let _year = year != nil ? year : currentYear
        
        var components = DateComponents()
        components.day = lastDayInMonth(_month!, year: _year!)
        components.month = _month
        components.year = _year
        let date = Calendar.current.date(from: components)
        
        return date!
    }
    
    static func dateInMonthsFromNow(_ startDate:Date! = nil, months:Int) -> Date {
        var _startDate:Date! = startDate
        if startDate == nil {
            _startDate = Date()
        }
        
        return (Calendar.current as NSCalendar).date(byAdding: .month, value: months, to: _startDate, options: [])!
    }
    
    static func dateInDaysFromNow(_ startDate:Date! = nil, days:Int) -> Date {
        var _startDate:Date! = startDate
        if startDate == nil {
            _startDate = Date()
        }
        
        return (Calendar.current as NSCalendar).date(byAdding: .day, value: days, to: _startDate, options: [])!
    }
    
    static func monthStringForMonth(_ month:Int) -> String {
        let dateFormatter = DateFormatter()
        let monthString = dateFormatter.monthSymbols[month-1]
        return monthString
    }
    
    static func dayStringFromDate(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE dd MMMM"
        return dateFormatter.string(from: date)
    }
    
    func formatDateFromUTC_ServerString() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "YYYY-MM-dd"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    func formatDateFromUTC_Readable_DayMonth() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "dd MMM"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    func formatDateFromUTC_Readable_DayMonthYear() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone.current //TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "dd MMM, yyyy"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    func formatDateFromUTC_Readable_MonthYear() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "MMM, yyyy"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    func formatDateFromUTC_Readable_Month() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "MMM"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    func formatDateFromUTC_Readable_Day() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "dd"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    func formatDateFromUTC_Readable_T() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone.current //TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "h:mm a"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    func formatDateFromUTC_Readable_T_NoAMPM() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormat.dateFormat = "HH:mm"
        dateFormat.timeZone = timeZone
        
        return dateFormat.string(from: self)
    }
    
    
    func formatDate_Readable_Month() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        dateFormat.dateFormat = "MMM"
        return dateFormat.string(from: self)
    }
    
    func formatDate_Readable_Day() -> String {
        let dateFormat:DateFormatter = DateFormatter()
        dateFormat.dateFormat = "dd"
        return dateFormat.string(from: self)
    }
    
    func asTimestamp() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        return dateFormatter.string(from: self)
    }
    
    func asDayOfWeekString_Short() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        let day = dateFormatter.string(from: self)
        
        return "\(day)"
    }
    
    func daySuffix() -> String {
        let calendar = Calendar.current
        let dayOfMonth = (calendar as NSCalendar).component(.day, from: self)
        switch dayOfMonth {
        case 1: fallthrough
        case 21: fallthrough
        case 31: return "st"
        case 2: fallthrough
        case 22: return "nd"
        case 3: fallthrough
        case 23: return "rd"
        default: return "th"
        }
    }
    
    func asDayDateWithSuffixString_Short() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE d"
        let day = dateFormatter.string(from: self)
        let suffix = self.daySuffix()
        
        return "\(day)\(suffix)"
    }
    
    func asTimeReadableString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: self)
    }
    
    func asFriendlyStringWithTime() -> String {
        let calendar : Calendar = Calendar.current
        let age = (calendar as NSCalendar).components([.minute, .hour, .day],
                                                      from: self,
                                                      to: Date(),
                                                      options: [])
        
        let timeString = self.asTimeReadableString()
        
        // Today
        if age.day == 0 {
            
            // Within 1 Hour
            if age.hour == 0 {
                
                // Right Now
                if age.minute == 0 {
                    return "Just now"
                }
                
                // 1 Minute Ago
                if age.minute == 1 {
                    return "1 minute ago"
                }
                
                // Minutes Ago
                return "\(age.minute ?? 0) minutes ago"
            }
            
            // 1 Hour Ago
            if age.hour == 1 {
                return "1 hour ago"
            }
            
            // Up To 6 Hours Ago
            if age.hour! < 6 {
                return "\(age.hour ?? 0) hours ago"
            }
            
            return "Today at \(timeString)"
        }
        
        // Yesterday
        if age.day == 1 {
            return "Yesterday at \(timeString)"
        }
        
        // Days Or More Ago
        let dateString = age.day! < 7 ? self.asDayOfWeekString_Short() : self.asDayDateWithSuffixString_Short()
        return "\(dateString) at \(timeString)"
    }
    
    static func fromTimestamp(string: String) -> Date! {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        return dateFormatter.date(from: string)
    }
}
