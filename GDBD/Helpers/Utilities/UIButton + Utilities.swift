//
//  UIButton + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 18/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

extension UIButton {
    func setUpAsDotButton(isGood: Bool) {
        self.setUpAsCircle()
        self.backgroundColor = isGood ? Theme.goodColour : Theme.badColour
        self.layer.borderColor = isGood ? Theme.goodBorderColour.cgColor : Theme.badBorderColour.cgColor
        self.layer.borderWidth = 2
        self.setTitle(nil, for: .normal)
    }
    
    func setUpAsBasicButton() {
        self.backgroundColor = Theme.basicButtonColour
        self.setTitleColor(UIColor.black, for: .normal)
    }
    
    func setUpAsDisabledButton() {
        self.backgroundColor = Theme.basicButtonColour
        self.setTitleColor(UIColor.black, for: .normal)
    }
    
    func setUpAsCommitButton(isGood: Bool) {
        self.backgroundColor = isGood ? Theme.goodColour : Theme.badColour
        self.setTitleColor(UIColor.white, for: .normal)
    }
    
    func setUpAsLandingPageButton() {
        self.backgroundColor = UIColor.white
        self.layer.borderColor = UIColor.black.cgColor
        self.setTitleColor(UIColor.black, for: .normal)
    }
}

