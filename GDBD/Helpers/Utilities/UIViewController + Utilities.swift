//
//  UIViewController + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 1/3/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

extension UIViewController {
    func setUpWithModalTopBar(titleText: String!) {
        let topBar = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 64))
        topBar.backgroundColor = Theme.goodColour
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 20, width: self.view.frame.width, height: 44))
        titleLabel.setupAsTitleLabel()
        titleLabel.text = titleText
        titleLabel.textAlignment = .center
        topBar.addSubview(titleLabel)
        let closeButton = UIButton(frame: CGRect(x: 10, y: 30, width: 34, height: 34))
        closeButton.addTarget(self, action: #selector(closeButtonDidPress_forSetUpWithModalTopBar), for: .touchUpInside)
        closeButton.setTitle("X", for: .normal)
        topBar.addSubview(closeButton)
        
        self.view.addSubview(topBar)
    }
    
    @objc fileprivate func closeButtonDidPress_forSetUpWithModalTopBar() {
        self.dismiss(animated: true, completion: nil)
    }
}

