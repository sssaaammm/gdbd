//
//  UIFont + Utilities.swift
//  GDBD
//
//  Created by Samuel Wong on 14/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

enum FontSize: Int {
    case Small = 8
    case Medium = 10
    case Large = 12
    
    var cgFloat: CGFloat {
        return CGFloat(self.rawValue)
    }
}

enum FontWeight {
    case Light
    case Regular
    case Bold
    
    var weight: UIFont.Weight {
        switch self {
        case .Light : return UIFont.Weight.light
        case .Regular : return UIFont.Weight.medium
        case .Bold : return UIFont.Weight.bold
        }
    }
}

extension UIFont {
    static func standardFont(size: FontSize = .Medium, weight: FontWeight = .Regular) -> UIFont {
        return UIFont.systemFont(ofSize: size.cgFloat, weight: weight.weight)
        
    }
}
