//
//  UIView + Utilities.swift
//  GDBD
//
//  Created by Samuel Wong on 14/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import pop

extension UIView {
    func loadViewFromNib(_ nibName:String, forClass aClass: AnyClass! = nil, atIndex index:Int = 0) {
        let bundle:Bundle! = aClass != nil ? Bundle(for: aClass) : nil
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[index] as! UIView
        
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.backgroundColor = UIColor.clear
        addSubview(view)
        
        view.layoutIfNeeded()
        view.layoutSubviews()
    }
    func animateToHidden(isHidden: Bool, isDelayed delay: CFTimeInterval = 0) {
        let showAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha)!
        showAnimation.toValue = isHidden ? 0 : 1
        showAnimation.beginTime = CACurrentMediaTime() + delay
        self.pop_add(showAnimation, forKey: "showDotButton")
    }
    
    func performLayout() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    func setUpAsSeperator() {
        self.backgroundColor = Theme.seperatorColour
    }
    
    func setUpAsCircle() {
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
    }
}
