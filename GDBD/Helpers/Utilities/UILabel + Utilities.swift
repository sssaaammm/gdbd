//
//  UILabel + Utilities.swift
//  GDBD
//
//  Created by Samuel Wong on 14/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

extension UILabel {
    func setupAsStandardLabel() {
        self.font = UIFont.standardFont()
    }
    
    func setupAsTabBarLabel() {
        self.setupAsStandardLabel()
        self.numberOfLines = 0
        self.textAlignment = .center
    }
    
    func setupAsTitleLabel() {
        
    }
}
