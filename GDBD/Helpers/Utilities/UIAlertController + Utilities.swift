//
//  UIAlertController + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 30/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func showAlertWithError(viewController:UIViewController, error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }

    
    static func showAlertWithError(viewController: UIViewController, errorString: String) {
        let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
