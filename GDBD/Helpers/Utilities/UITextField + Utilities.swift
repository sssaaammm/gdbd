//
//  UITextField + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 29/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

extension UITextField {
    func setUpAsStandardTextField() {
        self.font = UIFont.standardFont()
        self.textAlignment = .left
    }
}
