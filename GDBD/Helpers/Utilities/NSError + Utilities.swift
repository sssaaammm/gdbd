//
//  Error + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 8/3/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

extension NSError {
    static func standardErrorWithString(errorString: String) -> NSError {
        return NSError(domain: Bundle.main.bundleIdentifier!, code: 0, userInfo: [NSLocalizedDescriptionKey : errorString])
    }
}
