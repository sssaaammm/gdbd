//
//  String + Utilities.swift
//  GDBD
//
//  Created by sarah burton on 8/3/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

extension String {
    static func isNilOrEmpty(string: String?) -> Bool {
        if string == nil {
            return true
        }
        if string == "" {
            return true
        }
        return false
    }
}
