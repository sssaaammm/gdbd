//
//  Constants.swift
//  GDBD
//
//  Created by sarah burton on 18/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

class Theme {
    static var goodColour = UIColor.colourWithHexString("82DE8C")
    static var goodBorderColour = UIColor.colourWithHexString("0C7026")
    static var badColour = UIColor.colourWithHexString("F16D71")
    static var badBorderColour = UIColor.colourWithHexString("AD2932")
    static var darkGrey = UIColor.colourWithHexString("BDBFC2")
    static var basicButtonColour = darkGrey
    static var basicButtonTextColour = UIColor.white
    static var seperatorColour = UIColor.black
    static var standardGrey = UIColor.colourWithHexString("DEE1E3")
    static var backgroundColour = UIColor.white
}

class Strings {
    static var month = "Month"
    static var year = "Year"
    static var delete = "Delete"
    static var missing_fields = "Please Provide Details"
    
    static var allStrings: [String] = [month, year, delete, missing_fields]
    static func printAllStrings() {
        for string in allStrings {
            print (string)
        }
    }
}

class APIKey {
    static var title = "title"
    static var isGood = "is_good"
    static var username = "username"
    static var password = "password"
    static var password1 = "password1"
    static var password2 = "password2"
    static var key = "key"
    static var id = "id"
    static var created = "created"
}

class APIStrings {
    static var saving = "Saving..."
}
