//
//  Storage.swift
//  BMW
//
//  Created by Samuel Wong on 3/10/2016.
//  Copyright © 2016 Samuel Wong. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
import SwiftyJSON

//MARK: API Access Token
struct Storage {
    private static let KEYCHAIN_KEY_ACCESS_TOKEN = "accessToken"
    private static let USER_DEFAULTS_KEY_ACCESS_TOKEN_DATE = "accessTokenDate"
    
    /**
     Saves the API Access Token to the Keychain & the current date to NSUserDefaults
     
     - parameter accessToken: Access Token to be saved
     */
    static func saveAccessToken (accessToken: String!) {
        if KeychainWrapper.standard.set(accessToken, forKey: KEYCHAIN_KEY_ACCESS_TOKEN) {
            UserDefaults.standard.set(NSDate(), forKey: USER_DEFAULTS_KEY_ACCESS_TOKEN_DATE)
        } else {
            print("Error: Unable to Save Access Token to the Keychain.")
        }
    }
    
    /**
     Retreives the API Access Token from the Keychain & its creation date from NSUserDefaults if it has them
     
     - returns: (token, creationDate) Tuple of the stored token. Returns optional values
     */
    static func accessToken () -> (token: String?, creationDate: NSDate?) {
        let token = KeychainWrapper.standard.string(forKey: KEYCHAIN_KEY_ACCESS_TOKEN)
        let creationDate = UserDefaults.standard.object(forKey: USER_DEFAULTS_KEY_ACCESS_TOKEN_DATE) as! NSDate?
        
        return (token, creationDate)
    }
    
    /**
     Clears the API Access Token from the Keychain & its creation date from NSUserDefaults if it has them
     */
    static func removeAccessToken () {
        if KeychainWrapper.standard.removeObject(forKey: KEYCHAIN_KEY_ACCESS_TOKEN) {
            print("Access Token has been Removed from the Keychain.")
            UserDefaults.standard.removeObject(forKey: USER_DEFAULTS_KEY_ACCESS_TOKEN_DATE)
        } else {
            print("Error: Unable to Remove Access Token from the Keychain.")
        }
    }
}
