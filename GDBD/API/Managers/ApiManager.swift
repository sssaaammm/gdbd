//
//  ApiManager.swift
//  BMW
//
//  Created by Samuel Wong on 2/09/2016.
//  Copyright © 2016 Samuel Wong. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let API_INFO_ERROR = "api_error"
let API_INFO_ERROR_TITLE = "api_error_title"
let API_INFO_ERROR_DESCRIPTION = "api_error_description"

enum ErrorStrings: String {
    case ErrorKey = "error"
    case DescriptionKey = "error_description"
}

class ApiManager {
    static func performRequest(withPath path:String? = nil, withLiteralPath:String? = nil, method: ApiHelper.Method, jsonEncoding:Bool = false, log:Bool = false, parameters: Dictionary<String, Any>?, forceExpectArray:Bool = false, completion:@escaping (_ json:JSON?, _ error:NSError?) -> Void) {
        let endpoint = withLiteralPath == nil ? ApiHelper.baseURL + path! : withLiteralPath!
        print(endpoint)
        var urlRequest = URLRequest(url: URL(string: endpoint)!)
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("", forHTTPHeaderField: "Cookie")
        if let accessToken = User.currentUser._apiToken {
            urlRequest.addValue("Token \(accessToken)", forHTTPHeaderField: "Authorization")
            print (accessToken)
        }
        
        do {
            
            urlRequest = !jsonEncoding ? try URLEncoding.default.encode(urlRequest, with: parameters) : try JSONEncoding.default.encode(urlRequest, with: parameters)

            Alamofire.request(urlRequest)
                .validate()
                .responseJSON { JSONresponse in
                    
                    let response = JSONresponse.response
                    let result = JSONresponse.result
                    let request = JSONresponse.request
                    
                    let error = result.error
                    var apiError:[String: AnyObject] = [:]
                    
                    //Error Returned
                    if result.isFailure || error != nil {
                        //TODO: Check error
                        if let data = JSONresponse.data {
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            if json == nil || json == "" {
                                return completion(nil, error as NSError?)
                            } else {
                                return completion(nil, NSError(domain: Bundle.main.bundleIdentifier!, code: 0, userInfo: [NSLocalizedDescriptionKey : json!]))
                            }
                        }
                        return completion(nil, NSError(domain: Bundle.main.bundleIdentifier!, code: 0, userInfo: ["Error" : apiError]))
                    }
                    
                    //No Response or Data Was Returned
                    if response == nil {
                        //TODO: Check error
                        return completion(nil, NSError(domain: Bundle.main.bundleIdentifier!, code: 0, userInfo: ["Error" : apiError]))
                    }
                    
                    var json:JSON!
                    
                    //TODO: Check this: result.value
                    if let data = result.value ?? result.value {
                        json = JSON(data)
                    }
                    
                    let code = response!.statusCode
                    
                    if let statusCode = ApiHelper.StatusCode(rawValue: code) {
                        switch statusCode {
                        case ._200_SUCCESS, ._201_CREATED, ._202_ACCEPTED :
                            if log { print(statusCode.description) }
                            
                            if json == nil {
                                print("No JSON data")
                                //Incase data is nil, it wount appear to fail
                                json = JSON(["success" : statusCode.code])
                            }
                            
                        case ._404_NOT_FOUND :
                            if log { print(statusCode.description) }
                            
                            apiError[API_INFO_ERROR_TITLE] = ApiHelper.StatusCode._404_NOT_FOUND.title as AnyObject
                            apiError[API_INFO_ERROR_DESCRIPTION] = ApiHelper.StatusCode._404_NOT_FOUND.description as AnyObject
                            
                            return completion(nil, NSError(domain: Bundle.main.bundleIdentifier!, code: statusCode.code, userInfo: [API_INFO_ERROR : apiError]))
                            
                        default :
                            if log { print(statusCode.description) }
                            
                            //JSON Data Returned with an Error
                            if let _ = json[ErrorStrings.ErrorKey.rawValue].string {
                                apiError[API_INFO_ERROR_TITLE] = json[ErrorStrings.ErrorKey.rawValue].string as AnyObject
                                apiError[API_INFO_ERROR_DESCRIPTION] = json[ErrorStrings.DescriptionKey.rawValue].string as AnyObject
                                
                            } else {
                                apiError[API_INFO_ERROR_TITLE] = statusCode.title as AnyObject
                                apiError[API_INFO_ERROR_DESCRIPTION] = statusCode.description as AnyObject
                            }
                            
                            return completion(nil, NSError(domain: Bundle.main.bundleIdentifier!, code: statusCode.code, userInfo: [API_INFO_ERROR : apiError]))
                        }
                    } else {
                        print("code : \(code)")
                        //Unhandled Return Code
                        if code < 200 || code >= 300 {
                            apiError[API_INFO_ERROR_TITLE] = "Server Error" as AnyObject
                            apiError[API_INFO_ERROR_DESCRIPTION] = "The server responded with an unknown error.\nError Code \(code)" as AnyObject?
                            
                            return completion(nil, NSError(domain: Bundle.main.bundleIdentifier!, code: code, userInfo: [API_INFO_ERROR : apiError]))
                        }
                    }
                    
                    //Final catch if the server responds success with an error in JSON
                    if json != nil {
                        if let error = json[ErrorStrings.ErrorKey.rawValue].string {
                            if log { print("Catch Error : \(error)") }
                            
                            var apiError:[String: AnyObject] = [:]
                            apiError[API_INFO_ERROR_TITLE] = json[ErrorStrings.ErrorKey.rawValue].string as AnyObject
                            apiError[API_INFO_ERROR_DESCRIPTION] = nil
                            return completion(nil, NSError(domain: Bundle.main.bundleIdentifier!, code: code, userInfo: [API_INFO_ERROR : apiError]))
                        }
                    }
                    //Success
                    if method == .POST {
                        return completion(json!,nil)
                    }
                    
                    if json.arrayValue.count > 0 {
                        if forceExpectArray {
                            return completion(json, nil)
                        } else {
                            return json.count == 1 ? completion(json[0], nil) : completion(json, nil)
                        }
                        
                    } else {
                        return completion(json,nil)
                    }
            }
        } catch {
            print("SOME ERROR IN APIMANAGER")
        }
        
    }
}
