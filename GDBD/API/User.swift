//
//  User.swift
//  GDBD
//
//  Created by sarah burton on 29/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit

private var _currentUser: User = User()

class User {
    static var currentUser: User {
        get {
            return _currentUser
        }
        set {
            _currentUser = newValue
        }
    }
    var _apiToken : String?
    
    var dots : [Dot] = []
}

extension User {
    static func logIn(username: String, password: String, completion: @escaping (_ error: Error?) -> Void) {
        var parameters: [String: Any] = [:]
        parameters[APIKey.username] = username.lowercased()
        parameters[APIKey.password] = password
        
        ApiManager.performRequest(withPath: "rest-auth/login/", method: .POST, parameters: parameters) { (json, error) in
            guard error == nil else { return completion(error) }
            if json != nil {
                if !String.isNilOrEmpty(string: json![APIKey.key].stringValue) {
                    User.currentUser._apiToken = json![APIKey.key].stringValue
                    Storage.saveAccessToken(accessToken: User.currentUser._apiToken)
                    return completion(nil)
                } else {
                    return completion(NSError.standardErrorWithString(errorString: "Empty JSON"))
                }
            } else {
                return completion(NSError.standardErrorWithString(errorString: "Empty JSON"))
            }
        }
    }
    
    static func signUp(username: String, password: String, completion: @escaping (_ error: Error?) -> Void) {
        var parameters: [String: Any] = [:]
        parameters[APIKey.username] = username.lowercased()
        parameters[APIKey.password1] = password
        parameters[APIKey.password2] = password
        
        ApiManager.performRequest(withPath: "rest-auth/registration/", method: .POST, parameters: parameters) { (json, error) in
            return completion(error)
        }
    }
    
    func fetchDots(completion: @escaping (_ error: Error?) -> Void) {
        ApiManager.performRequest(withPath: "dots/", method: .GET, parameters: nil) { (json, error) in
            guard error == nil else { return completion(error) }
            if json != nil {
                User.currentUser.dots = []
                for dotjson in json! {
                    print (dotjson.1)
                    User.currentUser.dots.append(Dot(json: dotjson.1))
                }
                return completion(nil)
            } else {
                return completion(nil)
            }
        }
    }
}
