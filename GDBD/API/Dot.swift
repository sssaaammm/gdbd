//
//  Dot.swift
//  GDBD
//
//  Created by sarah burton on 29/1/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import SwiftyJSON

class Dot {
    var id: Int!
    var isGood: Bool!
    var eventDetails: String!
    var date: Date!
    
    init () {}
    init (json: JSON) {
        self.mapJSON(json: json)
    }
}

extension Dot {
    func create(_ completion: @escaping (_ error: Error?) -> Void) {
        var parameters: [String: Any] = [:]
        parameters[APIKey.title] = eventDetails
        parameters[APIKey.isGood] = isGood
        
        ApiManager.performRequest(withPath: "dots/", method: .POST, parameters: parameters) { (json, error) in
            guard error == nil else { return completion(error) }
            if json != nil {
                self.mapJSON(json: json!)
                User.currentUser.dots.append(self)
            }
            return completion(nil)
        }
    }
    
    func delete(_ completion: @escaping (_ error: Error?) -> Void) {
        ApiManager.performRequest(withPath: "dots/\(id!)/", method: .DELETE, parameters: nil) { (json, error) in
            guard error == nil else { return completion(error) }
            if let index = User.currentUser.dots.index(where: { $0.id == self.id }) {
                User.currentUser.dots.remove(at: index)
            }
            return completion(nil)
        }
    }
}

extension Dot {
    func mapJSON(json: JSON) {
        self.isGood = json[APIKey.isGood].boolValue
        self.eventDetails = json[APIKey.title].stringValue
        self.id = json[APIKey.id].intValue
        self.date = Date.fromTimestamp(string: json[APIKey.created].stringValue)
        
        print (json)
    }
}
