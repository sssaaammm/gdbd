//
//  LandingViewController.swift
//  GDBD
//
//  Created by sarah burton on 8/3/18.
//  Copyright © 2018 Samuel Wong. All rights reserved.
//

import UIKit
import SVProgressHUD
import pop

class LandingViewController: GDBDViewController {

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var fieldsContainingView: UIView!
    @IBOutlet weak var logoViewVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateLandingPage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func initialise() {
        super.initialise()
        logInButton.addTarget(self, action: #selector(logInButtonDidPress), for: .touchUpInside)
        signUpButton.addTarget(self, action: #selector(signUpButtonDidPress), for: .touchUpInside)
        
        fieldsContainingView.alpha = 0
        
        userNameField.autocorrectionType = .no
        
        userNameField.placeholder = "User Name"
        passwordField.placeholder = "Password"
        
        logInButton.setTitle("Log In", for: .normal)
        logInButton.setUpAsLandingPageButton()
        signUpButton.setTitle("Sign Up", for: .normal)
        signUpButton.setUpAsLandingPageButton()
    }
}

extension LandingViewController {
    func animateLandingPage() {
        let logoAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
        logoAnimation.toValue = -2*self.view.frame.height/5 + logoView.frame.height/2 + 30
        logoViewVerticalConstraint.pop_add(logoAnimation, forKey: "moveItUp")
        
        if let accessToken = Storage.accessToken().token {
            SVProgressHUD.show(withStatus: "Loading...")
            User.currentUser._apiToken = accessToken
            User.currentUser.fetchDots(completion: { (error) in
                SVProgressHUD.dismiss()
                guard error == nil else { UIAlertController.showAlertWithError(viewController: self, error: error!); self.fieldsContainingView.animateToHidden(isHidden: false, isDelayed: 0.3); return }
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
                    self.present(vc, animated: true, completion: nil)
                }
            })
        } else {
            fieldsContainingView.animateToHidden(isHidden: false, isDelayed: 0.3)
        }
    }
}

extension LandingViewController {
    @objc func signUpButtonDidPress() {
        guard !String.isNilOrEmpty(string: userNameField.text) else {
            UIAlertController.showAlertWithError(viewController: self, errorString: "User Name Field Cannot be Empty")
            return
        }
        guard !String.isNilOrEmpty(string: passwordField.text) else {
            UIAlertController.showAlertWithError(viewController: self, errorString: "Password Field Cannot be Empty")
            return
        }
        
        SVProgressHUD.show(withStatus: "Signing Up...")
        User.signUp(username: userNameField.text!, password: passwordField.text!) { (error) in
            SVProgressHUD.dismiss()
            guard error == nil else { UIAlertController.showAlertWithError(viewController: self, error: error!); return }
            self.logInButtonDidPress()
        }
    }
    
    @objc func logInButtonDidPress() {
        guard !String.isNilOrEmpty(string: userNameField.text) else {
            UIAlertController.showAlertWithError(viewController: self, errorString: "User Name Field Cannot be Empty")
            return
        }
        guard !String.isNilOrEmpty(string: passwordField.text) else {
            UIAlertController.showAlertWithError(viewController: self, errorString: "Password Field Cannot be Empty")
            return
        }
        
        SVProgressHUD.show(withStatus: "Logging In...")
        User.logIn(username: userNameField.text!, password: passwordField.text!) { (error) in
            guard error == nil else { SVProgressHUD.dismiss(); UIAlertController.showAlertWithError(viewController: self, error: error!); return }
            User.currentUser.fetchDots(completion: { (error) in
                SVProgressHUD.dismiss()
                guard error == nil else { UIAlertController.showAlertWithError(viewController: self, error: error!); return }
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() {
                    self.present(vc, animated: true, completion: nil)
                }
            })
        }
    }
}
